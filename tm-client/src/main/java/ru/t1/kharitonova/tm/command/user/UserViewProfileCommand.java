package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.user.UserProfileRequest;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "View current user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final User user = getAuthEndpoint().profile(request).getUser();
        if (user == null) return;
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID : " + user.getId());
        System.out.println("LOGIN : " + user.getLogin());
        System.out.println("FIRST NAME : " + user.getFirstName());
        System.out.println("MIDDLE NAME : " + user.getMiddleName());
        System.out.println("LAST NAME : " + user.getLastName());
        System.out.println("EMAIL : " + user.getEmail());
        System.out.println("ROLE : " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
