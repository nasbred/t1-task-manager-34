package ru.t1.kharitonova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractResultResponse;
import ru.t1.kharitonova.tm.dto.response.AbstractUserResponse;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable final String token) {
        super(token);
    }

}
