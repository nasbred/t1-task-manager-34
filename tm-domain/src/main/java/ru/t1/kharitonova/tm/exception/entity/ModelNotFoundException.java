package ru.t1.kharitonova.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityException {

    public ModelNotFoundException() {
        super("Error! Model not found.");
    }

}
