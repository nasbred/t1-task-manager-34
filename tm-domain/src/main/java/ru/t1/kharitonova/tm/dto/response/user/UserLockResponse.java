package ru.t1.kharitonova.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.response.AbstractUserResponse;
import ru.t1.kharitonova.tm.model.User;

public final class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

    public UserLockResponse(@Nullable final String token) {
        super(token);
    }

}
